import type { Schema, Attribute } from '@strapi/strapi';

export interface BureauList extends Schema.Component {
  collectionName: 'components_bureau_lists';
  info: {
    displayName: 'List';
    icon: 'bulletList';
    description: '';
  };
  attributes: {
    name: Attribute.String & Attribute.Required;
    membres: Attribute.Relation<
      'bureau.list',
      'oneToMany',
      'api::membre.membre'
    >;
  };
}

export interface LinksSocial extends Schema.Component {
  collectionName: 'components_clubs_socials';
  info: {
    displayName: 'Social';
    icon: 'link';
    description: '';
  };
  attributes: {
    link: Attribute.String & Attribute.Required;
    social: Attribute.Relation<
      'links.social',
      'oneToOne',
      'api::supported-social.supported-social'
    >;
  };
}

export interface SlideshowEvents extends Schema.Component {
  collectionName: 'components_slideshow_events';
  info: {
    displayName: 'Events';
    description: '';
  };
  attributes: {
    logo: Attribute.Media<'images'> & Attribute.Required;
    description: Attribute.Text & Attribute.Required;
    title: Attribute.String & Attribute.Required;
    cover: Attribute.Media<'images'> & Attribute.Required;
    link: Attribute.String;
  };
}

export interface SlideshowSlideshow extends Schema.Component {
  collectionName: 'components_home_slideshows';
  info: {
    displayName: 'Home';
    icon: 'picture';
    description: '';
  };
  attributes: {
    image: Attribute.Media<'images', true> & Attribute.Required;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'bureau.list': BureauList;
      'links.social': LinksSocial;
      'slideshow.events': SlideshowEvents;
      'slideshow.slideshow': SlideshowSlideshow;
    }
  }
}
