'use strict';

/**
 * bureau controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::bureau.bureau');
