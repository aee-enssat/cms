'use strict';

/**
 * bureau router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::bureau.bureau');
