'use strict';

/**
 * event-hero service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::event-hero.event-hero');
