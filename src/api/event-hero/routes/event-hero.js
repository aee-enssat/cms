'use strict';

/**
 * event-hero router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::event-hero.event-hero');
