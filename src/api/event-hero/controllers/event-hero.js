'use strict';

/**
 * event-hero controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::event-hero.event-hero');
