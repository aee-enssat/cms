'use strict';

/**
 * supported-social router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::supported-social.supported-social');
