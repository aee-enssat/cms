'use strict';

/**
 * supported-social controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::supported-social.supported-social');
