'use strict';

/**
 * supported-social service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::supported-social.supported-social');
