
# AEE-ENSSAT CMS Repository

![AEE-ENSSAT](https://img.shields.io/badge/ENSSAT-AEE-blue)
![Strapi](https://img.shields.io/badge/Strapi-4.2.0-purple)
![Docker](https://img.shields.io/badge/Docker-available-blue)

Bienvenue sur le dépôt GitLab du CMS de l'Association des Élèves de l'ENSSAT (AEE). Ce projet est centré sur le système de gestion de contenu (CMS) utilisé pour alimenter le site web de l'AEE et faciliter l'administration des contenus.

## Aperçu du CMS

![Screenshot du CMS](https://cdn.bde-enssat.fr/img/strapi.png)
*Tableau de bord du CMS Strapi*

## Technologies Utilisées

Le projet utilise des technologies modernes pour gérer les contenus de manière efficace et flexible :

- **Strapi** : CMS Headless pour la gestion des contenus dynamiques, permettant une administration facile et une intégration avec divers frontends.
- **Docker** : Utilisé pour simplifier le déploiement et l'exécution du CMS.

## Configuration et Lancement du Projet

### Prérequis

Assurez-vous d'avoir les éléments suivants installés sur votre machine :

- Node.js (version >= 12.x)
- npm
- Docker

### Étapes pour exécuter le projet

1. **Cloner le dépôt :**

   ```bash
   git clone https://gitlab.com/aee-enssat/cms.git
   ```

2. **Naviguer dans le répertoire du projet :**

   ```bash
   cd cms
   ```

3. **Installer les dépendances :**

   ```bash
   npm install
   ```

4. **Configuration du fichier `.env` :**

   Copiez le fichier `.env.example` en `.env` et complétez les variables requises. Voici un exemple des variables que vous devrez définir :

   ```bash
   DATABASE_URL=postgres://user:password@localhost:5432/database
   ```

5. **Lancer le serveur de développement :**

   ```bash
   npm run develop
   ```

   Le CMS sera alors accessible à l'adresse [http://localhost:1337](http://localhost:1337).

---

### Utilisation d'un environnement de développement local

#### Prérequis
Vous devez avoir docker installé sur votre machine ainsi que la commande `make`. \
Si vous n'avez pas la commande make faites \
`apt-get -y install make` en root

#### Utilisation

Placez vous à la racine du projet puis faites `make dev`

### Utilisation de Docker

Une image Docker est disponible pour exécuter le projet. Vous pouvez la récupérer et l'exécuter avec les commandes suivantes :

```bash
docker pull aee-enssat/cms
docker run -p 1337:1337 aee-enssat/cms
```

Cela démarrera le CMS sur le port 1337.

---

### Contribution

Pour apporter des modifications ou des améliorations, veuillez contacter le responsable technique et infrastructure de l'AEE actuelle à l'adresse suivante : [informatique@bde-enssat.fr](mailto:informatique@bde-enssat.fr).

---

**Liens Importants :**
- [Dépôt GitLab](https://gitlab.com/aee-enssat/cms)
- [Site Web de l'AEE](https://bde-enssat.fr)

Pour toute question ou assistance supplémentaire, n'hésitez pas à contacter les mainteneurs du projet via les issues du dépôt ou par email.
